"""Class that represents the network to be evolved."""
import random
import logging
from network import *
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import EarlyStopping
from sklearn.metrics import confusion_matrix
from keras.utils import multi_gpu_model
import numpy as np





class MLP_Model(Network):
	"""Represent a MLP network and let us operate on it.
	"""

	def __init__(self, nn_param_choices, architecture):
		"""Initialize our network.
		"""
		self.performance = {}
		super(MLP_Network, self).__init__(nn_param_choices = nn_param_choices, architecture = architecture)
		self.nn_param_choices = nn_param_choices

		

	def compile_MLP_model(self, dataset):
		"""Compile a sequential model.

		Args:

		Returns:
			a compiled network.

		"""
		# Get our network parameters.
		no_layers = self.architecture['no_layers']
		no_neurons = self.architecture['no_neurons']
		activation_Fn1 = self.architecture['activation_fn_1']
		activation_Fn2 = self.architecture['activation_fn_2']
		optimizer = self.architecture['optimizer']
		InitMode=self.architecture['initmode']
		dropout = self.architecture['dropout']
		model = Sequential()

		# Add each layer.
		for i in range(nb_layers):

			# Need input shape for first layer.
			if i == 0:
				model.add(Dense(nb_neurons, activation=activation1,kernel_initializer=InitMode , input_shape=input_shape))
			else:
				model.add(Dense(nb_neurons, activation=activation1,kernel_initializer=InitMode ))

			model.add(Dropout(dropout))  

		# Output layer.
		model.add(Dense(nb_classes, activation=activation2,kernel_initializer=InitMode ))

		model.compile(loss='categorical_crossentropy', optimizer=optimizer,
					metrics=['accuracy'])

		return model






